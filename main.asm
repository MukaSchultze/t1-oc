# Trabalho 1 - Organiza��o de Computadores (ELC1011)
# Professor Giovani Baratto
# Jogo da forca - V1.0
# 26/05/2018
#
# Autores
# Samuel Schultze
# Gustavo Kessler

.eqv SCREEN_SIZE 65536 # Tamanho da mem�ria ocupado pelo display
.eqv BUFFER_LENGTH 1024 # Tamanho do buffer do arquivo 

# Aloca um registrador na pilha
.macro push (%reg)
    addiu $sp, $sp, -4
    sw    %reg, 0($sp)
.end_macro

# Restaura um registrador da pilha
.macro pull (%reg)
    lw    %reg, 0($sp)
    addiu $sp, $sp, 4
.end_macro

# Sai do programa com o c�digo
.macro quit(%exitCode)
    li   $v0, 17
    li   $a0, %exitCode
    syscall	
.end_macro

.text
main: 
    jal  fixKeyboardAndDisplay
    
    # L� o arquivo de palavras
    la  $a0, words_filepath
    la  $a1, words_buffer
    li  $a2, BUFFER_LENGTH
    jal readFile
    
    jal setRandomWord
    jal loadStringHit

    la  $a0, sprite_background # Desenha o cen�rio na tela
    jal renderBitmap
    
    li  $s4, 0 # Inicia o contador de erros
    # Loop de jogadas
    playAgain:
    jal playRound
    j   playAgain

# Executa uma unica jogada
playRound:
    push ($ra)

    bge  $s4, 6, gameOver # Caso o numero de erros seja igual a 6 perde o jogo

    la   $a0, string_digiteCaractere
    jal  printString # Printa a string digiteCaractere 

    la   $a0, playingWord_hit
    jal  printString # Printa as letras acertaadas
    
    jal  readChar # Le um caractere na nossa ferramenta keyboard
    move $a0, $v0 # Move o retorno do readChar para o argumento do replace char
    move $s3, $v0 # Move o retorno para uso posterior
    jal  replaceChar # Substitui as ocorrencias do caractere na string de letras acertadas

    la    $a0, playingWord
    move  $a1, $s3
    jal   stringContainsChar # Verifica quantas ocorrencias do caractere h� na palavra sorteada
    bge   $v0, 1, playRound_if_1 # Pula para playRound_if_1 se o jogador tiver acertado uma letra
    addiu $s4, $s4, 1 # Incrementa o numero de errors
    la    $a0, string_wrong
    jal   printString # Imprime uma mensagem dizendo que errou 
    playRound_if_1:

    # Desenha cada parte do nosso querido boneco
    la  $a0, sprite_background
    
    # Desenha as partes do enforcado
    beq   $s4, 0, playRound_switch_end
    la  $a0, sprite_head 
    beq   $s4, 1, playRound_switch_end
    la  $a0, sprite_body 
    beq   $s4, 2, playRound_switch_end
    la  $a0, sprite_arm_left 
    beq   $s4, 3, playRound_switch_end
    la  $a0, sprite_arm_right 
    beq   $s4, 4, playRound_switch_end
    la  $a0, sprite_leg_left 
    beq   $s4, 5, playRound_switch_end
    la  $a0, sprite_leg_right
    playRound_switch_end:
    jal renderBitmap

    # Verifica se ainda h� '-' nas letras acertadas
    la   $a0, playingWord_hit
    li   $a1, '-'
    jal  stringContainsChar
    beq  $v0, 0, gameWin # Se n�o houverem '-' o jogador ganhou

    pull ($ra)
    jr   $ra

gameOver:
    la   $a0, string_palavraCerta
    jal  printString 
    la   $a0, playingWord
    jal  printString # Imprime a palavra correta
    la   $a0, string_gameOver
    jal  printString # Imprime uma mensagem dizendo que perdeu 
    quit(0) # Sai do programa
    j    gameOver

gameWin:
    la   $a0, playingWord_hit
    jal  printString # Imprime a palavra correta
    la   $a0, string_gameWin
    jal  printString # Imprime uma mensagem dizendo que ganhou
    quit(0) # Sai do programa
    j    gameWin

# Substitui todas as ocorrencias de $a0 da palavra sorteada nas letras acertadas
# $a0 = Char to replace
replaceChar:
    push ($ra)
    push ($s0)
    move  $s0, $a0 # Faz uma c�pia do argumento passado
    la    $a0, playingWord # Carrega o endere�o da palavra sorteada
    jal   strLen # Verifica o numero de caracteres na palavra
    li    $t0, 0 # Inicia o contador do loop em 0
    replaceChar_loop:
    lb    $t1, playingWord($t0) # Carrega o caractere da palavra sorteada
    bne   $t1, $s0, replaceChar_loop_if
    sb    $t1, playingWord_hit($t0) # Salva o caractere nas letras acertadas se for igual a $a0
    replaceChar_loop_if:
    addiu $t0, $t0, 1 # Incrementa o contador
    blt   $t0, $v0, replaceChar_loop # Repete o loop enquanto contador < numero de letras
    pull ($s0)
    pull ($ra)
    jr    $ra

# Procedimento que corrige bug na ferramenta keyboard and display simulator
fixKeyboardAndDisplay:
    li   $t0, 1 # Set flag of 0xFFFF0008
    sw   $t0, 0xFFFF0008
    jr   $ra

# Sorteia uma palavra para jogar
setRandomWord:
    push ($ra)
    la    $a0, words_buffer # Carrega o endere?o do conteudo do arquivo
    li    $a1, '\n' # Carrega o cactere 
    jal   stringContainsChar # Verifica o numero de ocorrencias de \n no arquivo
    move  $s0, $v0 # Move o resultado para $s0
    pull ($ra)
   
    li $v0, 42 # Gera um numero aleat�rio entre 0 e $s0 e salva em $a0
    li $a0, 1
    la $a1, 0($s0)
    syscall

    la    $t0, words_buffer # Carrega o endere�o do conteudo do arquivo
    li    $t1, 0 # Inicia o index da palavra em 0
    setRandomWord_loop:
    beq   $t1, $a0, setRandomWord_end # Se o index da palavra atual for o mesmo do random, quebramos o loop
    lb    $t2, 0($t0) # Carrega o cactere o arquivo
    addiu $t0, $t0, 1 # Incrementa o endere�o
    bne   $t2, '\n', setRandomWord_loop # Se n�o for \n continua o loop
    addiu $t1, $t1, 1 # Incrementa o index da palavra se for uma nova linha
    j     setRandomWord_loop # Continua o loop
    setRandomWord_end:
    la    $a0, 0($t0) # Seta os argumentos do procedimento seguinte
    la    $a1, playingWord
    push($ra)
    jal   copyWord # Copia a palavra do endere� $t0 para o endere�o da palavra sorteada
    pull($ra)
    jr    $ra

# $a0 = FileName
renderBitmap:
    push($ra)

    la  $a1, sprite_buffer # L� o arquivo passado como argumento
    li  $a2, SCREEN_SIZE
    jal readFile

    li   $t0, 0
    fillScreen_loop:
    lw   $t1, sprite_buffer($t0) # Copia os bytes do buffer para a tela
    sw   $t1, screen($t0)
    addiu $t0, $t0, 4
    blt $t0, SCREEN_SIZE, fillScreen_loop
    
    pull($ra)
    jr    $ra

# $a0 = Filename
# $a1 = Buffer address
# $a2 = Buffer size
readFile:
    move $t1, $a1
    move $t2, $a2

    # Abre o arquivo para leitura
    li   $v0, 13
    li   $a1, 0
    li   $a2, 0
    syscall 
    move $t3, $v0 # Move o descritor de arquivo para $t3

    # Le o arquivo
    li   $v0, 14
    move $a0, $t3 # Move o descritor de arquivo para o argumento 
    move $a1, $t1 # Endere�o do buffer
    move $a2, $t2 # Tamanho do buffer
    syscall

    # Fecha o arquivo
    li   $v0, 16
    move $a0, $t3 # Move o descritor de arquivo para o argumento 
    syscall
    jr    $ra

# $a0 = Address of string
strLen:
    li    $v0, 0 # Inicia o contador em 0
    strLen_loop:
    lb    $t0, 0($a0) # Carrega o caractere
    beq   $t0, '\0', strLen_end # Fim da string caso char == '\0'
    beq   $t0, '\n', strLen_end # Fim da string caso char == '\n'
    beq   $t0, '\r', strLen_end # Fim da string caso char == '\r'
    addiu $a0, $a0, 1 # Incrementa endere�o
    addiu $v0, $v0, 1 # Incrementa contador
    move  $t6, $t0
    j     strLen_loop # Continua o loop
    strLen_end:
    jr   $ra

# Verifica o numero de ocorrencias na string
# $a0 = Address of string
# $a1 = Char to compare
# $v0 = result
stringContainsChar:
    li    $v0, 0 # Inicia o contador de ocorrencias em 0
    stringContainsChar_loop:
    lb    $t0, 0($a0) # Carrega o caractere no endere�o
    addiu $a0, $a0, 1 # Incrementa o endere?o
    beq   $t0, '\0', stringContainsChar_end # Se tivermos atingido o fim da string quebra o loop
    bne   $t0, $a1, stringContainsChar_loop # Continua o loop se o caractere for != do argumento
    addiu $v0, $v0, 1 # Incrementa o contador de ocorrencias
    j     stringContainsChar_loop # Continua o loop
    stringContainsChar_end:
    jr    $ra

# Copia uma string de $a0 para $a1
#$a0 = Adress of src
#$a1 = Adress of dst
copyWord:
    lb    $t0, 0($a0) # Carrega o caractere
    sb    $t0, 0($a1) # Salva o caractere
    addiu $a0, $a0, 1 # Incrementa o endere?o fonte
    addiu $a1, $a1, 1 # Incrementa o endere?o destino
    beq   $t0, '\n', copyWord_end # Quebra o loop se char == '\n'
    beq   $t0, '\r', copyWord_end # Quebra o loop se char == '\r'
    beq   $t0, '\0', copyWord_end # Quebra o loop se char == '\0'
    j     copyWord # Continua loop
    copyWord_end:
    li    $t0, '\0' # Salva um '\0' no fim da string
    sb    $t0, 0($a1)
    jr    $ra

# Inicia a string de letras acertadas com valores de '-'
loadStringHit:
    push ($ra)
    la    $a0, playingWord
    jal   strLen # Verifica o tamanho da palavra sorteada
    li    $t0, 0 # Inicia o contador em 0
    li    $t1, '-' # Carrega o caractere para inserir
    loadStringHit_loop:
    sb    $t1, playingWord_hit($t0) # Salva o caractere na string
    addiu $t0, $t0, 1 # Incrementa o contador
    blt   $t0, $v0, loadStringHit_loop # Continua o loop se o contador < numero de letras
    pull ($ra)
    jr    $ra

# Imprime um caractere na ferramenta keyboard and display simulator
# $a1 = Char to print
printChar:
    lb    $t0, 0xFFFF0008 # Verifica se o display est� disponivel para escrita
    beq   $t0, 0, printChar
    sb    $a1, 0xFFFF000C # Escreve o caractere
    jr    $ra

# Imprime uma string na ferramenta keyboard and display simulator
# $a0 = Address os string to print
printString:
    push($ra)
    printString_loop:
    lb    $a1, 0($a0) # Carrega o caractere no endere�o passado
    beq   $a1, '\0', printString_end # Se for o final da string quebra o loop
    jal   printChar # Imprime o caractere
    addiu $a0, $a0, 1 # Incrementa o endere�o
    j     printString_loop # Continua o loop
    printString_end:
    # Adiciona uma quebra de linha no display
    li    $a1, '\r' 
    jal   printChar
    li    $a1, '\n'
    jal   printChar
    pull($ra)
    jr    $ra
    
# L� um caractere na ferramenta keyboard and display simulator
# $v0 = Char read
readChar:
    lb    $t0, 0xFFFF0000 # Espera um caractere ser digitado
    beq   $t0, 0, readChar
    lb    $v0, 0xFFFF0004 # L� o caractere digitado
    bgt   $v0, 'z', readChar # L� novamente se n�o for uma letra
    blt   $v0, 'A', readChar
    bge   $v0, 'a', readChar_end # Muda a letra maiuscula para minuscula
    addiu $v0, $v0, 32
    readChar_end:
    jr    $ra

.data 0x10010000
    screen: .space SCREEN_SIZE
    sprite_buffer: .space SCREEN_SIZE

    # Cria Strings de intera��o com o usu�rio
    string_wrong: .asciiz "ERRROUUUUUUUU!!!!!!!!!"
    string_palavraCerta: .asciiz "\nA palavra certa era"
    string_gameOver: .asciiz "Voc� perdeu e matou um pobre inocente\n"
    string_gameWin: .asciiz "Voc� ganhou e acabou de salvar um grande homem\n"
    string_digiteCaractere: .asciiz "\nDigite uma letra na nossa ferramenta keyboard and display simulator: "

    # String de Controles do jogo
    playingWord: .space 32
    playingWord_hit: .space 32

    # Strings de controle dos arquivos
    words_buffer: .space BUFFER_LENGTH
    words_filepath: .asciiz "words.txt"

    # Arquivos de bitmap
    sprite_background: .asciiz "background.bin"
    sprite_head: .asciiz "head.bin"
    sprite_body: .asciiz "body.bin"
    sprite_arm_left: .asciiz "arm_left.bin"
    sprite_arm_right: .asciiz "arm_right.bin"
    sprite_leg_left: .asciiz "leg_left.bin"
    sprite_leg_right: .asciiz "leg_right.bin"